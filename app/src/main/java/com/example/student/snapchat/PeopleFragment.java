package com.example.student.snapchat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.util.ArrayList;

public class PeopleFragment extends Fragment {

    private ArrayList<String> people;
    private ArrayAdapter<String> peopleListAdapter;

    public PeopleFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_people, container, false);

        final Uri imageToSend = getActivity().getIntent().getParcelableExtra("ImageUri");

        Button addPeopleButton = (Button) view.findViewById(R.id.addpeopleButton);
        addPeopleButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("Add A Person");

                final EditText inputField = new EditText(getActivity());
                alertDialog.setView(inputField);

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //the users canceled do nothing
                    }
                });
                alertDialog.setPositiveButton("Add Person", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendFriendRequest(inputField.getText().toString());
                        Toast.makeText(getActivity(), "Add Person!", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.create();
                alertDialog.show();
            }
        });
        people = new ArrayList<String>();
        peopleListAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                people
        );
        final ListView peopleListView = (ListView) view.findViewById(R.id.peopleList);
        peopleListView.setAdapter(peopleListAdapter);
        String currentUser = Backendless.UserService.loggedInUser();
        Backendless.Persistence.of(BackendlessUser.class).findById(currentUser, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser user) {
                Object[] peopleObjects = (Object[]) user.getProperty("people");

                if(peopleObjects.length > 0){
                    BackendlessUser[] peopleArray = (BackendlessUser[]) peopleObjects;
                    for (BackendlessUser person : peopleArray){
                        String name = person.getProperty("name").toString();
                        people.add(name);
                        peopleListAdapter.notifyDataSetChanged();
                    }
                }
                final String currentUserName = user.getProperty("name").toString();
                peopleListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                        String personName = (String) parent.getItemAtPosition(position);
                        sendImageToFriend(currentUserName, personName, imageToSend);

                }
                });

            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });
        return view;
    }

    private void sendImageToFriend(String currentUser, String toUser, Uri imageUri){
        //start a service to send a picture for currentuser to toUser
        Intent intent = new Intent(getActivity(), SnapChatService.class);
        intent.setAction(Constants.ACTION_SEND_PHOTO);
        intent.putExtra("fromUser", currentUser);
        intent.putExtra("toUser", toUser);
        intent.putExtra("imageUri", imageUri);
        getActivity().startService(intent);


    }

    private void sendFriendRequest(final String personName){
        String  currentUserID = Backendless.UserService.loggedInUser();
        Backendless.Persistence.of(BackendlessUser.class).findById(currentUserID, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser currUser) {
                Intent intent = new Intent(getActivity(), SnapChatService.class);
                intent.setAction(Constants.ACTION_SEND_FRIEND_REQUEST);
                intent.putExtra("fromUser", currUser.getProperty("name").toString());
                intent.putExtra("toUser",personName);


                getActivity().startService(intent);
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });
    }
}
