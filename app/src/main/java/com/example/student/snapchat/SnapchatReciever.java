package com.example.student.snapchat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class SnapchatReciever extends BroadcastReceiver {
    public SnapchatReciever() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if(action.equals(Constants.BROADCAST_ADD_PERSON_SUCCESS)){
            Toast.makeText(context, "Added Person!", Toast.LENGTH_SHORT).show();
        } else if(action.equals(Constants.BROADCAST_ADD_PERSON_FAILURE)) {
            Toast.makeText(context, "Failed to add Person!", Toast.LENGTH_SHORT).show();
            }
        if(action.equals(Constants.BROADCAST_FRIEND_REQUEST_SUCCESS)){
            Toast.makeText(context, "Friend Request Sent!", Toast.LENGTH_SHORT).show();
        } else if(action.equals(Constants.BROADCAST_FRIEND_REQUEST_FAILURE)) {
            Toast.makeText(context, "Failed to send friend request!", Toast.LENGTH_SHORT).show();
            }
        }
    }

