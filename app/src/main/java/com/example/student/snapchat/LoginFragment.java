package com.example.student.snapchat;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link //LoginFragment.//OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#//newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment {


    private EditText loginUsername;
    private EditText loginPassword;
    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        Button button = (Button) view.findViewById(R.id.loginButton);
        loginUsername = (EditText) view.findViewById(R.id.loginUsername);
        loginPassword = (EditText) view.findViewById(R.id.loginPassword);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                String usernameField = loginUsername.getText().toString();
                String passwordField = loginPassword.getText().toString();
                //Register a user
                BackendlessUser backendlessUser = new BackendlessUser();
                backendlessUser.setPassword(passwordField);
                backendlessUser.setProperty("name", usernameField);

                Backendless.UserService.login(usernameField, passwordField,new AsyncCallback<BackendlessUser>() {
                    @Override
                    public void handleResponse(BackendlessUser response) {
                        Toast.makeText(getActivity(),"You logged in!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Toast.makeText(getActivity(),"User not logged in!", Toast.LENGTH_SHORT).show();
                    }
                },
                true);
            }

        });
        /*String[] menuItemszx = {"Go To Camera", "Go To Messages", "Go To People"};

        ListView listView = (ListView) view.findViewById(R.id.loginMenu);
        ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                menuItemszx
        );

        listView.setAdapter(listViewAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Toast.makeText(getActivity(), "You clicked camera item", Toast.LENGTH_SHORT).show();
                    //Intent intent = new Intent(getActivity(), CameraActivity.class);
                    //startActivity(intent);
                } else if (position == 1) {
                    Toast.makeText(getActivity(), "You clicked messages item", Toast.LENGTH_SHORT).show();
                    //Intent intent = new Intent(getActivity(), MessagesActivity.class);
                    //startActivity(intent);
                }else if (position == 2) {
                    Toast.makeText(getActivity(), "You clicked people item", Toast.LENGTH_SHORT).show();
                    //Intent intent = new Intent(getActivity(), PeopleActivity.class);
                   // startActivity(intent);
                }

            }
        });*/
        return view;

    }
}

