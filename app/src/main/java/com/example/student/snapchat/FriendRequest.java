package com.example.student.snapchat;

/**
 * Created by Student on 6/1/2016.
 */
public class FriendRequest {

    private String toUser;
    private String fromUser;
    private boolean accepted;

    public FriendRequest(){
        toUser = "";
        fromUser = "";
        accepted = false;
    }

    public String getToUser(){
        return toUser;
    }

    public String setToUser(String user){
        toUser = user;
        return toUser;
    }
    public String getFromUser(){
        return fromUser;
    }

    public String setFromUser(String user){
        fromUser = user;
        return fromUser;
    }

    public boolean isAccepted(){
        return accepted;
    }
    public void setAccepted(boolean isAccepted){
        accepted = isAccepted;
    }


}
