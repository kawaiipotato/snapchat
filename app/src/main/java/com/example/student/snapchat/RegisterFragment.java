package com.example.student.snapchat;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;


public class RegisterFragment extends Fragment {

    private EditText registerUsername;
    private EditText registerPassword;
    public RegisterFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        Button button = (Button) view.findViewById(R.id.registerButton);
        registerUsername = (EditText) view.findViewById(R.id.registerUsername);
        registerPassword = (EditText) view.findViewById(R.id.registerPassword);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                String username = registerUsername.getText().toString();
                String password = registerPassword.getText().toString();
                //Register a user
                BackendlessUser backendlessUser = new BackendlessUser();
                backendlessUser.setPassword(password);
                backendlessUser.setProperty("name", username);

                Backendless.UserService.register(backendlessUser, new AsyncCallback<BackendlessUser>() {
                    @Override
                    public void handleResponse(BackendlessUser response) {
                        Toast.makeText(getActivity(),"You registered!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Toast.makeText(getActivity(),"User not registered!", Toast.LENGTH_SHORT).show();
                    }
                });
            }

        });
       /* String[] menuItemsz = {"Go To Camera", "Go To Messages", "Go To People"};

        ListView listView = (ListView)view.findViewById(R.id.registerMenu);
        ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                menuItemsz
        );

        listView.setAdapter(listViewAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    Toast.makeText(getActivity(), "You clicked camera item", Toast.LENGTH_SHORT).show();
                    //Intent intent = new Intent(getActivity(), CameraActivity.class);
                    //startActivity(intent);
                } else if(position == 1 ){
                    Toast.makeText(getActivity(), "You clicked messages item", Toast.LENGTH_SHORT).show();
                    //Intent intent = new Intent(getActivity(), MessagesActivity.class);
                    //startActivity(intent);
                }else if (position == 2) {
                    Toast.makeText(getActivity(), "You clicked people item", Toast.LENGTH_SHORT).show();
                    //Intent intent = new Intent(getActivity(), PeopleActivity.class);
                    //startActivity(intent);
                }

            }
        });
        // Inflate the layout for this fragment*/
        return view;

    }
}
