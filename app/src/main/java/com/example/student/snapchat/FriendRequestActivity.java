package com.example.student.snapchat;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class FriendRequestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_request);

        FriendRequestFragment friendRequestFragment = new FriendRequestFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, friendRequestFragment).commit();
    }

}
