package com.example.student.snapchat;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FriendRequestFragment extends Fragment {

    private ArrayList<String> fromUser;
    private ArrayList<FriendRequest> friendRequests;
    private ArrayAdapter<String> friendRequestAdapter;

    public FriendRequestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_people, container, false);
        fromUser = new ArrayList<String>();
        friendRequests = new ArrayList<FriendRequest>();
        friendRequestAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                fromUser);
        ListView peopleListView = (ListView) view.findViewById(R.id.peopleList);
        peopleListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                    showAcceptDialog(position);
                }

        });
        peopleListView.setAdapter(friendRequestAdapter);
        //get name of logged in user
        String userID = Backendless.UserService.loggedInUser();
        Backendless.UserService.findById(userID, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                String currentUserName = response.getProperty("name").toString();
                //Query FriendRequest table  to logged in User
                getIncommingFriendRequests(currentUserName);
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });


        return view;
            }

    private void showAcceptDialog(final int position){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setMessage("Accept Friend Request From " + fromUser.get(position) + "?");

        dialog.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                acceptRequest(friendRequests.get(position));
            }
        });
        dialog.create();
        dialog.show();

    }

    private void getIncommingFriendRequests(String username){
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setWhereClause(String.format("toUser = '%s'", username));

        Backendless.Persistence.of(FriendRequest.class).find(query, new AsyncCallback<BackendlessCollection<FriendRequest>>() {
            @Override
            public void handleResponse(BackendlessCollection<FriendRequest> response) {
                List<FriendRequest> incommingRequests = response.getData();
                for (FriendRequest request : incommingRequests) {
                    if (!request.isAccepted()) {
                        fromUser.add(request.getFromUser());
                        friendRequests.add(request);
                    }
                }
                friendRequestAdapter.notifyDataSetChanged();
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });
    }

    private void acceptRequest(final FriendRequest request){
        request.setAccepted(true);
        Backendless.Persistence.save(request, new AsyncCallback<FriendRequest>() {
            @Override
            public void handleResponse(FriendRequest response) {
                Intent intent = new Intent(getActivity(), SnapChatService.class );
                intent.setAction(Constants.ACTION_ADD_PERSON);
                intent.putExtra("firstUsername", request.getFromUser());
                intent.putExtra("secondUsername", request.getToUser());
                getActivity().startService(intent);
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });
        Toast.makeText(getActivity(), "Accept Request!", Toast.LENGTH_SHORT).show();
    }

    }
