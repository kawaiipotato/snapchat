package com.example.student.snapchat;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.files.BackendlessFile;
import com.backendless.persistence.BackendlessDataQuery;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SnapChatService extends IntentService {
    public SnapChatService() {
        super("SnapChatService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String action = intent.getAction();

            if (action.equals(Constants.ACTION_ADD_PERSON)) {
                String firstUserName = intent.getStringExtra("firstUserName");
                String secondUserName = intent.getStringExtra("secondUserName");
                Log.i("SnapChatservice", "Service adding person. First User: " + firstUserName + " Second User: " + secondUserName);
                addPersons(firstUserName, secondUserName);
            } else if(action.equals(Constants.ACTION_SEND_FRIEND_REQUEST)){
                String toUser = intent.getStringExtra("toUser");
                String fromUser = intent.getStringExtra("fromUser");
                Log.i("SnapChatService", "Send Friend Request to: " + toUser + " from User " + fromUser);
                sendFriendRequest(fromUser,toUser);
            } else if(action.equals(Constants.ACTION_SEND_PHOTO)){
                String toUser = intent.getStringExtra("toUser");
                String fromUser = intent.getStringExtra("fromUser");
                Uri imageUri = intent.getParcelableExtra("imageUri");
                sendPhoto(fromUser, toUser, imageUri);
            }
        }
    }

    private void sendPhoto(String fromUser, String toUser, Uri imageUri){
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
            String timestamp = new SimpleDateFormat("yyyyMMdd_HHss").format(new Date());
            String imageFileName = "JPEG_" + timestamp + "_.jpg";
            String imageDirectory = "sentPics/";

            final SentPicture sentPicture = new SentPicture();
            sentPicture.setToUser(toUser);
            sentPicture.setFromUser(fromUser);
            sentPicture.setImageLocation(imageDirectory + "/" + imageFileName);

            Backendless.Files.Android.upload(
                    bitmap,
                    Bitmap.CompressFormat.JPEG,
                    100,
                    imageFileName,
                    imageDirectory,
                    new AsyncCallback<BackendlessFile>() {
                        @Override
                        public void handleResponse(BackendlessFile response) {
                            Backendless.Persistence.save(sentPicture, new AsyncCallback<SentPicture>() {
                                @Override
                                public void handleResponse(SentPicture response) {

                                }

                                @Override
                                public void handleFault(BackendlessFault fault) {

                                }
                            });
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {

                        }
                    }
            );
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    private void sendFriendRequest(final String fromUser, final String toUser){
        Log.i("SnapChatService", "send friend request start");
        //make Sure to user exists
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setWhereClause(String.format("name = '%s'", toUser));
        Log.i("SnapChatService", "send friend request before handle response");
        Backendless.Persistence.of(Backendless.class).find(query, new AsyncCallback<BackendlessCollection<Backendless>>() {
            @Override
            public void handleResponse(BackendlessCollection<Backendless> user) {
                if (user.getData().size() == 0) {
                    Log.i("SnapChatService", "send friend request beginning of handle response if");
                    broadcastFriendRequestFailure();
                } else {
                    Log.i("SnapChatService", "send friend request beginning of handle response else");
                    //create friend request
                    FriendRequest friendRequest = new FriendRequest();
                    friendRequest.setToUser(toUser);
                    friendRequest.setFromUser(fromUser);
                    friendRequest.setAccepted(false);


                    //save friend request to backendless
                    Backendless.Persistence.save(friendRequest, new AsyncCallback<FriendRequest>() {
                        @Override
                        public void handleResponse(FriendRequest response) {
                            broadcastFriendRquestSuccess();
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            broadcastFriendRequestFailure();
                            broadcastFriendRequestFailure();
                        }
                    });
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.i("SnapChatService", "send friend request beginning of handle fault");
                broadcastFriendRequestFailure();
            }
        });

    }

    private void addPersons(String firstUserName, String secondUsername){
        //Find both users
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setWhereClause(String.format("name = '%s' or name = '%s'", firstUserName, secondUsername));
        Backendless.Persistence.of(BackendlessUser.class).find(query, new AsyncCallback<BackendlessCollection<BackendlessUser>>() {
            @Override
            public void handleResponse(BackendlessCollection<BackendlessUser> response) {
                List<BackendlessUser> users = response.getData();
                if(users.size()!= 2){
                    broadcastAddPersonFailure();
                } else {
                    //update firstUser, adding secondUser as friend
                    BackendlessUser user1 = users.get(0);
                    final BackendlessUser user2 = users.get(1);

                    //update secondUser, adding first user as a friend
                    updatePersonsList(user1, user2);
                    //Saves first user
                    Backendless.UserService.update(user1, new AsyncCallback<BackendlessUser>(){
                        //If saving succeeds updates firstPerson's friend list
                       @Override
                        public void handleResponse(BackendlessUser user){
                           //Log.i("SCS", "updating user1");
                            //Update second user, adding firstUser as person
                           updatePersonsList(user2, user);
                           //Saves second user and adds first to the friend list
                           Backendless.UserService.update(user2, new AsyncCallback<BackendlessUser>() {
                               @Override
                               public void handleResponse(BackendlessUser response) {
                                   //Log.i("SCS", "updating user2");
                                   broadcastAddPersonSuccess();
                               }

                               @Override
                               public void handleFault(BackendlessFault fault) {
                                   //Log.i("SCS", "user2: " + fault.toString());
                                    broadcastAddPersonFailure();
                               }
                           });
                       }

                       @Override
                       public void handleFault(BackendlessFault fault){
                          // Log.i("SCS", "user1: " + fault.toString());
                            broadcastAddPersonFailure();
                       }
                    });
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                //Log.i("SCS", "finding user: " + fault.toString());
                broadcastAddPersonFailure();
            }
        });
    }

    private void broadcastAddPersonSuccess(){
        Intent intent = new Intent(Constants.BROADCAST_ADD_PERSON_SUCCESS);
        sendBroadcast(intent);
    }

    private void broadcastAddPersonFailure(){
        Intent intent = new Intent(Constants.BROADCAST_ADD_PERSON_FAILURE);
        sendBroadcast(intent);
    }

    private void broadcastFriendRquestSuccess(){
        Intent intent = new Intent(Constants.BROADCAST_FRIEND_REQUEST_SUCCESS);
        sendBroadcast(intent);
    }

    private void broadcastFriendRequestFailure(){
        Intent intent = new Intent(Constants.BROADCAST_FRIEND_REQUEST_FAILURE);
        sendBroadcast(intent);
    }

    private void updatePersonsList(BackendlessUser user, BackendlessUser person){
        //TODO: update user person list to include person
        Object[] newPersonList;

        Object [] currentPersonObjects = (Object[]) user.getProperty("people");
        if(currentPersonObjects.length > 0){
            Log.i("SnapChatService","debug person logging");
            Object[] currentPeople = currentPersonObjects;
            newPersonList = new Object[currentPeople.length+1];
            for(int i = 0; i< currentPeople.length -1; i++){
                newPersonList[i] = currentPeople[i];
            }
            newPersonList[newPersonList.length -1] = person;
        } else{
            newPersonList = new Object[]{person};
        }
        user.setProperty("people", newPersonList);
    }
}
