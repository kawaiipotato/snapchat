package com.example.student.snapchat;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LoggedInMenuFragment extends Fragment {

    public LoggedInMenuFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_logged_in_menu, container, false);

        Button logOutButton = (Button) view.findViewById(R.id.logoutButton);
        logOutButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Backendless.UserService.logout(new AsyncCallback<Void>() {
                    @Override
                    public void handleResponse(Void response) {
                        Toast.makeText(getActivity(), "logged out", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Toast.makeText(getActivity(), "failed to log out", Toast.LENGTH_SHORT).show();
                    }
                });
            }

        });

        String[] menuItemsz = {"", "Go To People", "Go To Camera",  "Go To Messages", "Go To Friend Requests"};

        ListView listView = (ListView)view.findViewById(R.id.loggedInMenu);
        ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                menuItemsz
        );

        listView.setAdapter(listViewAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 2) {
                    Toast.makeText(getActivity(), "You clicked camera item", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), CameraActivity.class);
                    startActivity(intent);

                } else if (position == 0) {

                } else if (position == 1) {
                    Toast.makeText(getActivity(), "You clicked people item", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), PeopleActivity.class);
                    startActivity(intent);
                } else if (position == 3) {
                    Toast.makeText(getActivity(), "You clicked messages item", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), MessagesActivity.class);
                    startActivity(intent);
                }else if (position == 4) {
                    Toast.makeText(getActivity(), "You clicked friend request item", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), FriendRequestActivity.class);
                    startActivity(intent);
                }

            }
        });
        return view;
    }


}
