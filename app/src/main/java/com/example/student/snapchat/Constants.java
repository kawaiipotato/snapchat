package com.example.student.snapchat;

/**
 * Created by Student on 5/26/2016.
 */
public class Constants {

    public static final String ACTION_ADD_PERSON = "com.example.student.snapchat.ADD_PERSON";
    public static final String ACTION_SEND_FRIEND_REQUEST = "com.example.student.snapchat.SEND_FRIEND_REQUEST";
    public static final String ACTION_SEND_PHOTO = "com.example.student.snapchat.SEND_PHOTO";

    public static final String BROADCAST_ADD_PERSON_SUCCESS = "com.example.student.snapchat.ADD_PERSON_SUCCESS";
    public static final String BROADCAST_ADD_PERSON_FAILURE = "com.example.student.snapchat.ADD_PERSON_FAILURE";

    public static final String BROADCAST_FRIEND_REQUEST_SUCCESS = "com.example.student.snapchat.FRIEND_REQUEST_SUCCESS";
    public static final String BROADCAST_FRIEND_REQUEST_FAILURE = "com.example.student.snapchat.FRIEND_REQUEST_FAILURE";
}
