package com.example.student.snapchat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MessagesFragment extends Fragment {

    static final int REQUEST_CHOOSE_PHOTO = 2;

    public MessagesFragment() {
        // Required empty public constructor
    }
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view =  inflater.inflate(R.layout.fragment_messages, container, false);

        String[] menuItems = {"Send Picture"};

        ListView listView = (ListView)view.findViewById(R.id.messagesMenu);
        ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                menuItems
        );

        listView.setAdapter(listViewAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    Toast.makeText(getActivity(), "You clicked login item", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Photo"), REQUEST_CHOOSE_PHOTO);
                }
            }
        });

        return view;
    }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data){
            if(requestCode == REQUEST_CHOOSE_PHOTO){
                if(resultCode == Activity.RESULT_OK){
                    Uri uri = data.getData();
                    Intent intent = new Intent(getActivity(), PeopleActivity.class);
                    intent.putExtra("ImageURI", uri);
                    startActivity(intent);
                }
            }
        }
}
