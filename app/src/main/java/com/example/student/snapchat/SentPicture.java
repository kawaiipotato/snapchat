package com.example.student.snapchat;

/**
 * Created by Student on 6/2/2016.
 */
public class SentPicture {

    private boolean viewed;
    private String fromUser;
    private String toUser;
    private String imageLocation;

    public SentPicture(){
        viewed = false;
        fromUser = "";
        toUser = "";
        imageLocation = "";
    }

    public boolean isViewed(boolean wasViewed){
        viewed = wasViewed;
        return viewed;
    }

    public String getToUser(){
        return toUser;
    }

    public String setToUser(String user){
        toUser = user;
        return toUser;
    }

    public String getFromUser(){
        return fromUser;
    }

    public String setFromUser(String user){
        fromUser = user;
        return fromUser;
    }

    public String getImageLocation(){
        return imageLocation;
    }

    public void setImageLocation(String location){
        imageLocation = location;
    }



}
